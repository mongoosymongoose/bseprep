#include "flashcard.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct flashcard *flashcard_create(char *title, char *contents)
{

    // Validate parameters given. ---------------------------------------
    if (strlen(title) == 0)
    {
        // Title passed is empty. 
        fprintf(stderr, "flashcard_create: No title was passed\n");
        return NULL;
    }
    //printf("flashcard_create: Title: %d\n", strlen(title));
    if (strlen(contents) == 0)
    {
        // Contents was empty. 
        fprintf(stderr, "flashcard_create: No contents were passed\n");
        return NULL;
    }
    //printf("flashcard_create: Contents: %d\n", strlen(contents));
    // Allocate Space --------------------------------------------------------
    struct flashcard *temp = calloc(1, sizeof(struct flashcard));
    if (!temp)
    {
        // calloc failed. 
        fprintf(stderr, "flashcard_create: Unable to create flashcard\n");
        return NULL;
    }
    // Allocate space for the title. 
    temp->title = calloc(1, strlen(title));
    // Copy over the value from the user. 
    strncpy(temp->title, title, strlen(title));
    
    // Allocate space for the description
    temp->description = calloc(1, strlen(contents));
    // Copy over the value from the user. 
    strncpy(temp->description, contents, strlen(contents));
    return temp;
    
}

void flashcard_destroy(struct flashcard *flashcard)
{
    if (flashcard)
    {
        free(flashcard);
    }
}