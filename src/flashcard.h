#ifndef FLASHCARD_H
#define FLASHCARD_H

struct flashcard {
    char *title;
    char *description;
};

/**
 * Creates a new flashcard with the given title and contents by allocating
 * memory and assigned parameter values to it.
 *
 * Flashcard must be deallocated using the flashcard_destroy() function.
 *
 * @param title     Name of flashcard to use to uniquely identify it.
 * @param contents  Description of the flashcard.
 * @return          Pointer to created flashcard object, or NULL if an error
 *                  occurs.
 */
struct flashcard *flashcard_create(char *title, char *contents);

/**
 * Destroys a flashcard by deallocating all memory.
 *
 * @param flashcard Flashcard to destroy. Should not be used after destroying.
 * @return          None
 */
void flashcard_destroy(struct flashcard *flashcard);

#endif // FLASHCARD_H
