#include "flashcard.h"
#include "deck.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void deck_print(struct deck *deck);

struct deck *deck_create(void)
{
    struct deck *temp = calloc(1, sizeof(struct deck));
    if (!temp)
    {
        // Error with calloc. 
        fprintf(stderr, "Unable to create space for deck\n");
        return NULL;
    }
    
    temp->bottom_card = calloc(1, sizeof(struct card_node));
    if (!temp->bottom_card)
    {
        // Error with calloc. 
        fprintf(stderr, "Unable to create bottom_card\n");
        // Free already calloc'ed space. 
        free(temp);
        return NULL;
    }
    
    temp->top_card = calloc(1, sizeof(struct card_node));
    if (!temp->top_card)
    {
        // Error with calloc.
        fprintf(stderr, "Unable to create top_card\n");
        // Free already calloc'ed space. 
        free(temp->bottom_card);
        free(temp);
        return NULL;
    }
    
    // TODO Probabaly not the right way to do this part. 
    // NOTE bottom_card during circularly linked lists will always be NULL
    temp->bottom_card->flashcard = NULL;
    temp->bottom_card->next = NULL;
    temp->bottom_card->previous = NULL;
    temp->top_card->flashcard = NULL;
    temp->top_card->next = NULL;
    temp->top_card->previous = NULL;
    
    return temp; 
}

void deck_destroy(struct deck *deck)
{
    /** 
     * I'm going to go from the top and free as I go. 
     */
    if (!deck->bottom_card && !deck->top_card)
    {
        free(deck);
        return; 
    }
    
    // Make a temp deck to keep our place. 
    struct deck *temp = deck; 
    if (!temp)
    {
        // Error occured during creation. 
        fprintf(stderr, "deck_destroy: Error occured during deleting deck\n");
        return ;
    }

    while (temp->top_card->next)
    {
        // Move the top card to the next card. 
        temp->top_card = deck->top_card->next; 
        flashcard_destroy(temp->top_card->flashcard);
        free(deck); 
        
    }
    
}


/**
 * Inserts a flashcard into the deck. This will always add a card to the bottom
 * of the deck.
 *
 * @param flashcard Flashcard to insert into the deck, at the bottom of the
 * deck.
 * @return          0 on success, -1 on failure.
 */
int deck_insert_flashcard(struct flashcard *flashcard, struct deck *deck)
{
    if (!flashcard)
    {
        // Flashcard wasn't passed; can't add it. 
        fprintf(stderr, "deck_insert_flashcard: No flashcard passed\n");
        return -1;
    }
    
    if (!deck)
    {
        // Deck must have been created first. 
        fprintf(stderr, "deck_insert_flashcard: No deck was passed\n");
        // TODO figure out if this is where this should be done. 
        free(flashcard);
        return -1; 
    }
    
    // Copy over the deck. 
    struct deck *tmp = deck;
    
    if (deck->top_card->flashcard == NULL)
    {
        deck->top_card->flashcard = flashcard;
        deck->bottom_card = deck->top_card; 
        return 0;
    }
    
    // Set the current bottom_card's next to this flashcard. 
    struct card_node *tempCardNode = calloc(1, sizeof(struct card_node));
    if (!tempCardNode)
    {
        fprintf(stderr, "deck_insert_flashcard: Error occured while adding flashcard to deck\n");
        return -1; 
    }
    
    tempCardNode->flashcard = flashcard; 
    tempCardNode->next = deck->top_card; 
    tempCardNode->previous = deck->bottom_card;
    deck->top_card->previous = tempCardNode;
    deck->bottom_card->next = tempCardNode; 
    deck->bottom_card = tempCardNode; 
    
    
    return 0;
}

 
struct flashcard *deck_search(struct deck *deck, char *flashcard_title,
        int verbose_flag)
{
    // Check for a deck
    if (deck == NULL)
    {
        // Can't search for a card if there's no deck.  
        fprintf(stderr, "deck_search: No deck was given\n");
        return NULL;
    }
    
    if ((flashcard_title == NULL) || (strlen(flashcard_title) == 0))
    {
        fprintf(stderr, "deck_search: No flashcard title given\n");
        return NULL;
    }
    
    if ((verbose_flag > 1) || (verbose_flag < 0))
    {
        // Invalid flag passed. 
        fprintf(stderr, "deck_search: Invalid verbose_flag passed\n");
        return NULL;
    }
    
    if (deck->top_card->flashcard == NULL)
    {
        // No cards in the deck. 
        fprintf(stderr, "deck_search: Deck is empty\n");
        return NULL;
    }
    
    
    // At this point all basic check are done. 
    // Iterate through the deck looking for the card. 
    struct card_node *current_node = deck->top_card;

    struct flashcard *currentFlashcard = current_node->flashcard;
    char firstFlashcard[64]; 
    bool seenTwice = false; 
    strncpy(firstFlashcard, currentFlashcard->title, strlen(currentFlashcard->title));
    if (current_node == NULL)
    {
        // Error occured while creating current_flashcard
        fprintf(stderr, "deck_search: Error occured while searching deck\n");
        return NULL;
    }
    
    while (1)
    {

        if (verbose_flag == 1)
        {
            // If the user wants to print each card being checked...
            printf("'%s'\n", currentFlashcard->title);
        }
        
        if (strncmp(currentFlashcard->title, flashcard_title, strlen(flashcard_title)) == 0)
        {
            // Found the flashcard. Return it to the calling function. 
            return currentFlashcard;
        }
        
        // Check for if we've gone through the whole circle
        if (current_node == deck->bottom_card)
        {
            return NULL; 
        }
        
        // Iterate
        current_node = current_node->next; 
        currentFlashcard = current_node->flashcard;
    }
    
        
}

bool checkOrder(char *card1, char *card2)
{
    // Checks two flashcard titles for alphabetical order. 
    bool in_order = true; 
    int i = 0;
    while((i < strlen(card1)) && (i < strlen(card2)))
    {
        if (card1[i] == ' ' || card2[i] == ' ')
        {
            i++; 
            continue;
        }
        // Check
        if (card2[i] - card1[i] > 0)
        {
            return true;
        }
        else if (card2[i] - card1[i] < 0)
        {
            return false; 
        }
        
        // Iterate
        i++;
    }
    return true; 
    
    
}



/**
 * Draw a flashcard from the deck by removing it from the deck and returning a
 * pointer to it.
 *
 * The behavior of this function will change depending on the data structure
 * representing the deck's implementation. For example, in some cases, this
 * function may draw the bottom card (most recently added) to the deck, and in
 * other cases, may draw the bottom card.
 *
 * @param deck  Deck to draw card from.
 * @return      Flashcard drawn from the deck.
 */
struct flashcard *deck_draw_flashcard(struct deck *deck)
{
    if (deck == NULL)
    {
        fprintf(stderr, "deck_draw_flashcard: No deck passed\n");
        return NULL;
    }
    
    if (deck->top_card == NULL)
    {
        puts("This deck doesn't have any cards");
        return NULL;
    }
    
    return deck->top_card->flashcard;
}


/**
 * Sorts a deck in place so that the cards in it are in alphabetical order by
 * title.
 *
 * @param deck  Deck to sort.
 * @return      0 on success, -1 on failure.
 */
int deck_sort(struct deck *deck)
{
    // Sorting alphabetical
    if (deck == NULL)
    {
        fprintf(stderr, "deck_sort: No deck passed to sort\n");
        return -1; 
    }
    
    
    bool deck_in_order = false; 
    
    // For each node, check where it belongs. 
    struct card_node *currentCard = deck->top_card; 
    struct card_node *masterTracker = currentCard; 
    struct flashcard *currentFlashcard = currentCard->flashcard; 
    struct card_node *testCard = deck->top_card;
    struct flashcard *testFlashcard = testCard->flashcard; 
    bool reachedItself = false; 
    bool doneSorting = false;
    
    while (doneSorting == false)
    {
        if (currentCard == deck->bottom_card)
        {
            // We're at the last card, but we still want to check it. 
            struct flashcard *tmp = deck->bottom_card->flashcard; 
            if (checkOrder(currentCard->flashcard->title, testFlashcard->title) == false)//(checkOrder(deck->top_card->flashcard->title, currentFlashcard->title) == false)
            {
                return 0;
            }
            else
            {
                puts("In weird else");
                
            }
            continue; 
        }
        else if (currentCard == testCard)
        {
            if (reachedItself == false)
            {
                testCard = testCard->next; 
                testFlashcard = testCard->flashcard;
                reachedItself = true; 
                continue;
            }
            else
            {
                // todelete
                struct card_node *nextCard = currentCard->next; 
                struct flashcard *nextFlashcard = nextCard->flashcard; 
                struct card_node *nextNextCard = nextCard->next; 
                struct flashcard *nextNextFlashcard = nextNextCard->flashcard;
                currentCard = currentCard->next; 
                currentFlashcard = currentCard->flashcard; 
                testCard = currentCard->next->next; 
                testFlashcard = testCard->flashcard; 
                reachedItself = false; 
            }
        }
        
        if (checkOrder(currentFlashcard->title, testFlashcard->title) == true)
        {
            // In the right order.
            if (testCard == deck->bottom_card && masterTracker != deck->bottom_card)
            {
                currentCard->previous = masterTracker; 
                masterTracker = masterTracker->next; 
                currentCard = masterTracker; 
                currentFlashcard = currentCard->flashcard; 
                testCard = currentCard->next; 
                testFlashcard = testCard->flashcard; 
                
            }
            else if (masterTracker == deck->bottom_card)
            {
                //puts("DONE? ");
                return 0;
            }
            else 
            {    
                currentCard = masterTracker; 
                currentFlashcard= currentCard->flashcard; 
                testCard = testCard->next; 
                testFlashcard = testCard->flashcard;
            }
        }
        else
        {
            // Re-order. 
            // how
            struct flashcard *tempCurrentFlashcard = currentFlashcard; 
            struct card_node *tempCurrentCard = currentCard;
            currentCard->flashcard = testFlashcard; 
            
            bool setFlashcards = false;
            while (setFlashcards == false)
            {
                // Move one down. 
                if (tempCurrentCard->next->flashcard == testFlashcard)
                {
                    setFlashcards = true;
                }
                struct flashcard *t = tempCurrentCard->next->flashcard; 
                //printf("---> %s\n", t->title);
                tempCurrentCard->next->flashcard = tempCurrentFlashcard;
                tempCurrentFlashcard = t;
                tempCurrentCard = tempCurrentCard->next; 
            }
            
            
            if (currentCard == deck->top_card)
            {
                deck->bottom_card->next = currentCard;
            }
            else if (currentCard->next == deck->top_card)
            {
                deck->bottom_card = currentCard;
            }
            
            if (testCard == deck->bottom_card && masterTracker != deck->bottom_card)
            {
                masterTracker = masterTracker->next; 
                currentCard = masterTracker; 
                currentFlashcard = currentCard->flashcard; 
                testCard = currentCard->next; 
                testFlashcard = testCard->flashcard; 
                
            }
            else if (masterTracker == deck->bottom_card)
            {
                return 0;
            }
            else 
            {    
                currentCard = currentCard->next; 
                testCard = testCard->next; 
                testFlashcard = testCard->flashcard;
            }
        }
        
        
        
    }
    struct flashcard *top = deck->top_card->flashcard; 
    struct flashcard *bottom = deck->bottom_card->flashcard; 

    
}

void deck_print(struct deck *deck)
{
    struct card_node *currentCard = deck->top_card; 
    while (1)
    {
        struct flashcard *tmp = currentCard->flashcard; 
        printf("'%s | %s'\n", tmp->title, tmp->description);
        currentCard = currentCard->next; 
        if (currentCard == deck->top_card)
        {
            return;
        }
    }
}

/** 
  * Traverses the deck struct and returns how many cards there are. 
  * @param deck Deck to traverse. 
  * @return numCards The number of cards in the deck. -1 on failure. 
  */
int getNumCards(struct deck *deck)
{
    if (deck == NULL)
    {
        fprintf(stderr, "getNumCards: No deck passed\n");
        return -1;
    }
    
    if (deck->top_card == deck->bottom_card)
    {
        return 1; 
    }
    
    if (deck->top_card->next == deck->bottom_card)
    {
        return 2; 
    }
    
    struct card_node *tmp = deck->top_card;
    int numCards = 1;
    while (tmp != deck->bottom_card)
    {
        numCards++;
        tmp=tmp->next;
    }
    numCards++;
    return numCards;
}

int swap(struct deck *deck, int i, int randNum)
{
    // Check. 
    if (deck == NULL)
    {
        fprintf(stderr, "swap: Unable to shuffle deck\n");
        return -1; 
    }
    
    if (i <0 || randNum < 0)
    {
        fprintf(stderr, "swap: Unable to shuffle deck\n");
        return -1; 
    }
    // find card nodes that need to be swapped. 
    struct card_node *cardA = deck->top_card; 
    struct card_node *cardB = deck->top_card;
    int cardASpot = 0; 
    int cardBSpot = 0;
    while (1)
    {
        // Move cardA 
        if (cardASpot <= i)
        {
            cardA = cardA->next; 
        }
        if (cardBSpot <= randNum)
        {
            cardB = cardB->next; 
        }
        if (cardASpot > i && cardBSpot > randNum)
        {
            break;
        }
        
        cardASpot++; 
        cardBSpot++;
        
    }
    
    struct flashcard *tmp = cardA->flashcard;
    cardA->flashcard = cardB->flashcard;
    cardB->flashcard = tmp;
    return 0;
}



/**
 * Shuffles a deck in place so that the cards are randomly ordered.
 *
 * @param deck  Deck to shuffle.
 * @return      0 on success, -1 on failure.
 */
int deck_shuffle(struct deck *deck)
{
    // o How to randomize. 
    // 
    // swap(card[i], card[r])
    
    // Get the number of cards; 
    int numCards = getNumCards(deck);
    for (int i = 0; i < numCards; i++)
    {
        // One node at a time, swap it with a random card at randInt location. 
        int randNum = rand() % numCards;
        int test = swap(deck, i, randNum);
        if (test == -1)
        {
            fprintf(stderr, "deck_shuffle: Unable to shuffle deck\n");
            return -1; 
        }
    }
    return 0;
}