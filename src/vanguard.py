"""
	Project: OOP Python project for BN and Company Personnel
"""
import argparse
import Battalion


def main():
    """ The vanguard program allows for different arguments to get passed in
          setting different parameters if passed. Then a Battalion object is
          created, and altered, as well as a Company sub-class"""
    parser = argparse.ArgumentParser(description='User can provide the number \
    of Companies in the Battalion, the name of the Commander and / or CSM, \
    and / or initial number of soldiers. ')
    parser.add_argument('-n', type=int, help='the number of companies for the \
    battalion', required=False)
    parser.add_argument('-b', help='the name of the BN Commander. MUST be \
    in quotes', required=False)
    parser.add_argument('-c', help='the name of the BN CSM. MUST be in quotes',
                        required=False)
    parser.add_argument('-i', type=int, help='the number of soldiers the BN \
    starts with', required=False)
    args = parser.parse_args()

    num_companies = 1
    commander = "LTC Naley"
    csm = "CSM Potts"
    num_soldiers = 0

    if args.n:
        if args.n < 0:
            print("Invalid number of companies passed")
            return
        else:
            num_companies = args.n
            print("Number of companies to create: {}".format(args.n))

    if args.b:
        commander = args.b 
        print("Name for BN Commander: {}".format(args.b))

    if args.c:
        csm = args.c
        print("Name for CSM:{}".format(args.c))

    if args.i:
        if args.i < 0:
            print("Invalid number of soldiers passed")
            return
        else:
            num_soldiers = args.i


    # Create initial Battalion Object.
    vanguard = Battalion.Battalion("781 MI BN", commander, csm, num_companies,
    num_soldiers)

    # Print Command Team.
    print(vanguard.print_command_team())

    # Print BN Info
    print(vanguard.print_bn_info())

    # Adding Soldiers to the BN
    print("\nInprocessing new soldiers to the BN...")
    vanguard.add_soldiers(54)
    print("Vanguard now has {} soldiers\n".format(vanguard.get_num_soldiers()))

    # Create a Company
    alpha = Battalion.Company("A Co, 781 MI BN", "CPT Something",
    "1SG AndAnother", 0)
    print(alpha.print_company_info())
    
    # Add Soldiers to alpha
    print("\nInproccessing new soldiers to alpha...")
    alpha.add_soldiers(25)
    print(alpha.print_company_info())
    
    # Change of Command
    print("\nChange of Command occured....")
    vanguard.change_of_command("LTC NewPerson")
    print(vanguard.print_command_team())
    
    # Change of Responsibility
    print("\nChange of Responsibility occured...")
    vanguard.change_of_responsibility("CSM Hooah")
    print(vanguard.print_command_team())


if __name__ == "__main__":
    try:
        main()
    # Catches interrupts and errors not accounted for elsewhere.
    except (KeyboardInterrupt, Exception):
        print("\nProgram encountered problem. Exiting...")
