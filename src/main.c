/**
 * Program should use the deck and flashcard modules to represent a deck of
 * flashcards that allows a user to interact with the deck. The functionality
 * defined in the Requirements section of the README must be exposed to the
 * user in an interactive prompt in this main module, but leverage the data
 * structures that you create in the flashcard and deck modules.
 *
 * Feel free to create any additional helper modules or functions that may
 * assist your development.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "deck.h"
#include "flashcard.h"


// Function declarations
int create_a_deck(void);
struct deck *load_deck(FILE *fp);

int main(int argc, char **argv)
{
    int status = -1;    // Program exit status.
    struct deck *user_deck = NULL;
    /* Initialize deck */
    struct deck *deck = deck_create();
    if (NULL == deck)
    {
        fprintf(stderr, "Error initializing deck\n");
        goto done;
    }

    /* Begin command loop to receive user commands and interact with deck */
    char *menu = "Welcome to myFlashCards! Please select from the following menu: \n\tL) Load an existing deck\n\n\tX)Exit\n";
    char *secondMenu = "Select from the following options: \n\tD) Draw a card\n\tS) Search for a card\n\tP) Print\n\tA) Alphabetise \n\tR) Randomize \n\n\tX) Exit\n";
    printf("%s", menu);
    char input[64]; 
    fgets(input, 64, stdin);
    printf("You selected: %s\n", input);
    
    while (1)
    {
    
        switch(input[0])
        {
        case 'l':
        case 'L':
        {
            // User wants to create a new deck. 
            //int returnCode = create_a_deck();
            FILE *fp = fopen("palabras", "r");
            if (!fp)
            {
                fprintf(stderr, "Unable to open file\n");
                return -1; 
            }
            user_deck = load_deck(fp);
            break;
        }
        case 'x':
        case 'X':
        {
            puts("Goodbye!");
            return 0;
        }
        }
        
        break;
    
    }
    if (user_deck == NULL)
    {
        // SOmething happened while trying to load deck
        fprintf(stderr, "main: Error occured while trying to load deck\n");
        return -1;
    }
    while (input[0] != 'X' || input[0] != 'x')
    {
        // If the deck was created
        if (user_deck != NULL)
        {
            printf("%s", secondMenu);
            bzero(input, 64);
            fgets(input, 64, stdin);
        }
        switch(input[0])
        {
        case 's':
        case 'S':
        {
            // Get the card to search for
            bzero(input, 64);
            printf("Enter the card title to search for: ");
            fgets(input, 64, stdin);
            input[strlen(input) - 1] = '\0';
            printf("Do you want to see all the card titles as it searches? \n\tY) Yes\n\tN) No\n");
            char tmpInput[8];
            fgets(tmpInput, 8, stdin);
            int verbose = 0; 
            if (tmpInput[0] == 'y' || tmpInput[0] == 'Y')
            {
                verbose = 1;
            }
            
            struct flashcard *temp_flashcard = deck_search(user_deck, input, verbose);
            
            // If the card was found display it. 
            if (temp_flashcard != NULL)
            {
                printf("\n\t%s | %s\n\n", temp_flashcard->title, temp_flashcard->description);
            }
            else
            {
                // Otherwise let the user know. 
                printf("No card was found for '%s'\n", input);
            }
            break;
        }
        case 'p':
        case 'P':
        {
            deck_print(user_deck);
            break;
        }
        case 'a':
        case 'A':
        {
            status = deck_sort(user_deck);
            break;
        }
        case 'r':
        case 'R':
        {
            status = deck_shuffle(user_deck);
            break;
        }
        case 'd':
        case 'D':
        {
            struct flashcard *drawn_card = deck_draw_flashcard(user_deck);
            if (drawn_card != NULL)
            {
                printf("Title: \t\t%s\nHit <enter> to see reverse side\n", drawn_card->title);
                bzero(input, 64);
                fgets(input, 64, stdin);
                printf("Description: \t%s\n\n", drawn_card->description);
            }
            break;
        }
        case 'x':
        case 'X':
        case 'q':
        case 'Q':
        {
            puts("Goodbye!");
            goto done;
        }
        }
        
        
    }
    
    /* Set status to 0 if program executes successfully */
    status = 0;

    done:
    /* Cleanup deck and return exit status */
    deck_destroy(deck);
    return status;
}

int create_a_deck(void)
{
    // Create a new Deck!
    struct deck *newDeck = deck_create();
    if (!newDeck)
    {
        fprintf(stderr, "Error occured in create_a_deck\n");
        return -1; 
    }
    printf("Card title: ");
    char title[64]; 
    fgets(title, 64, stdin);
    title[strlen(title) - 1] = '\0';
    
    printf("Card Contents: ");
    char contents[64];
    fgets(contents, 64, stdin);
    contents[strlen(contents) - 1] = '\0';
    
    newDeck->top_card->flashcard = calloc(1, sizeof(struct flashcard));
    newDeck->top_card->flashcard = flashcard_create(title, contents);
        
}

struct deck *load_deck(FILE *fp)
{
    if (!fp)
    {
        fprintf(stderr, "No file passed");
        return NULL;
    }
    
    struct deck *tmp = deck_create();
    if (!tmp)
    {
        // Deck failed to get created. 
        fprintf(stderr, "load_deck: Deck creation failed\n");
        return NULL;
    }
    
    char *title = calloc(1, sizeof(64));
    char *description = calloc(1, sizeof(64));
    char buff[64];
    bzero(buff, 64);
    int pos =0; 
    bool isTitle = true; 
    //char currentChar = fgetc(fp);
    //fgets(buff, 64, fp);
    //struct flashcard *newCard = calloc(1, sizeof(struct flashcard));
    
    while (fgets(buff, 64, fp) != NULL)
    {
        if ((strncmp(buff, "\r\n", 2) == 0) || (strlen(buff) == 0))
        {
            bzero(buff, 64);
            isTitle = true;
            continue;
        }
        buff[strlen(buff) - 2] = '\0';
        //printf("%s\n", buff);

        if (isTitle == true)
        {
            strncpy(title, buff, strlen(buff));
            //printf("load_deck: Title: %s\n", title);
            
            
        }
        else
        {
            strncpy(description, buff, strlen(buff));
            //printf("load_deck: Description: %s\n", description);
            
            //printf("load_deck: %s | %s\n", title, description);
            
            // One at  a time to test. 
            struct flashcard *test = flashcard_create(title, description);
            
            int returnValue = deck_insert_flashcard(test, tmp);
            if (returnValue != 0)
            {
                fprintf(stderr, "load_deck: Error occured while inserting flashcard into deck\n");
                return NULL;
            }
            
            bzero(description, 64);
            bzero(title, 64);
        }

        isTitle = !isTitle;
        bzero(buff, 64);
        
    }
    
    return tmp;
    
}