""" Classes Battalion and Company are classes that have some different
     attributes for units in the army. """
class Battalion:
    """ Battalion Class that contains the command team attributes, a number of
         companies, and a number of soldiers. """
    def __init__(self, name, commander, csm, num_companies=0, num_soldiers=0):
        self.name = name
        self.commander = commander
        self.csm = csm
        self.num_companies = num_companies
        self.num_soldiers = num_soldiers

    def print_command_team(self):
        """ Prints the BN Command Team """
        return"BN Commander: {}\nBN CSM: {}".format(self.commander, self.csm)

    def print_bn_info(self):
        """ Prints all of the BN Info """
        return("{}\nBN Commander: {}\tBN CSM: {}\nNumber of Companies: {}\n\
        Number of Soldiers: {}\n".format(self.name, self.commander, self.csm,
                                         self.num_companies, self.num_soldiers))

    def change_of_command(self, new_commander):
        """ Sets new Commander name to commander."""
        self.commander = new_commander

    def change_of_responsibility(self, new_csm):
        """ Sets new CSM to self.csm"""
        self.csm = new_csm

    def add_soldiers(self, num_soldiers):
        """ Validates value passed to function then sets value of number of
             soldiers. """
        if num_soldiers < 1:
            print("Invalid number of soldiers passed")
            return
        self.num_soldiers = num_soldiers

    def get_num_soldiers(self):
        """ Returns the number of soldiers. """
        return(self.num_soldiers)

class Company(Battalion):
    """ Company is a sub-class of Battalion """
    def __init__(self, name, commander, top, num_soldiers=0):
        """ Sets the Company name, commander, 1SG, and number of soldiers. """
        self.name = name
        self.commander = commander
        self.top = top
        self.num_soldiers = num_soldiers

    def print_company_info(self):
        """ Returns the company information"""
        return("{}\nCommander: {}\nFirst Sergeant: {}\nSoldiers: {}".format(
            self.name, self.commander, self.top, self.num_soldiers))

    def add_soldiers(self, num_soldiers):
        """ Calls on the Parent Class's add_soldiers function"""
        super().add_soldiers(num_soldiers)

    def get_num_soldiers(self):
        """Calls on the Parent Class get_num_soldiers function """
        super().get_num_soldiers()
