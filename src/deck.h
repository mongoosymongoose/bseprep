#ifndef DECK_H
#define DECK_H

#include "flashcard.h"

/**
 * card_node struct should be modified according to the data structure being
 * implemented.
 */
struct card_node
{
    struct flashcard *flashcard;
    struct card_node *next;
	struct card_node *previous;
};

/**
 * deck struct should be modified according to the data structure being
 * implemented.
 */
struct deck
{
    struct card_node *bottom_card;
    struct card_node *top_card;
};


/**
 * Creates a new, empty deck object by allocating memory and initializing any
 * default values.
 *
 * Deck must be deallocated using the deck_destroy() function.
 *
 * @param   None
 * @return  Pointer to created deck object.
 */
struct deck *deck_create(void);

/**
 * Destroys a deck by destroying all flashcards in the deck and deallocating
 * space for the deck.
 *
 * @param deck  Deck to be destroyed.
 * @return      None
 */
void deck_destroy(struct deck *deck);

/**
 * Draw a flashcard from the deck by removing it from the deck and returning a
 * pointer to it.
 *
 * The behavior of this function will change depending on the data structure
 * representing the deck's implementation. For example, in some cases, this
 * function may draw the bottom card (most recently added) to the deck, and in
 * other cases, may draw the bottom card.
 *
 * @param deck  Deck to draw card from.
 * @return      Flashcard drawn from the deck.
 */
struct flashcard *deck_draw_flashcard(struct deck *deck);

/**
 * Inserts a flashcard into the deck. This will always add a card to the bottom
 * of the deck.
 *
 * @param flashcard Flashcard to insert into the deck, at the bottom of the
 * deck.
 * @param deck Deck where the Flashcard will be added to.
 * @return          0 on success, -1 on failure.
 */
int deck_insert_flashcard(struct flashcard *flashcard, struct deck *deck);


/**
 * Searches for a flashcard in a deck by the flashcard's title. If the
 * flashcard is not found because it is not in the deck, or if an error occurs,
 * return NULL.
 *
 * If the verbose_flag value is 1, then this function prints out all flashcard
 * titles that it discovers while searching. If the verbose_flag is 0, then
 * this function does not print while searching. For example, if searching for
 * a flashcard named "My Flashcard", and the deck contains the flashcards "Your
 * Flashcard", "Their Flashcard", "My Flashcard", and "No One's Flashcard", in
 * that order, then the titles of the first three flashcards should be printed
 * if the verbose_flag is set to 1 when this function is called.
 *
 * @param deck              Deck to search.
 * @param flashcard_title   Title of flashcard to search for.
 * @param verbose_flag      If set to 1, print out all flashcards come across
 *                          while searching for flashcard_title. If 0, ignore.
 * @return                  Pointer to flashcard if found, NULL if not found or
 *                          an error occurs.
 */
struct flashcard *deck_search(struct deck *deck, char *flashcard_title,
        int verbose_flag);

/**
 * Sorts a deck in place so that the cards in it are in alphabetical order by
 * title.
 *
 * @param deck  Deck to sort.
 * @return      0 on success, -1 on failure.
 */
int deck_sort(struct deck *deck);

/**
 * Shuffles a deck in place so that the cards are randomly ordered.
 *
 * @param deck  Deck to shuffle.
 * @return      0 on success, -1 on failure.
 */
int deck_shuffle(struct deck *deck);

/**
  * Prints Out all cards in deck. 
  * @param deck Deck to print.
  */
void deck_print(struct deck *deck);


#endif // DECK_H
