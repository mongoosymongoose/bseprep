# Data Structures

In this project, you will learn how to implement many data structures by
creating flash cards.

## Requirements

Implement a deck and cards to fill the deck. A deck can have many cards. Each
card should have two fields of text, a title and a description.

Users should be able to do the following:

* `draw` - User draws the top card and displays both fields of text to the
  console.
* `insert` - User adds a new card to the deck on the bottom of the deck.
* `sort` - User sorts the deck according to the title (no requirement on how
  the sort is performed, as long as it occurs in-place in memory).
* `shuffle` - User randomizes the order of cards.
* `search` - User looks through the deck for a card that matches the title.
  This should also print out the card immediately before this card and the card
  immediately following this card. Additionally, print out the number of nodes
  traversed during the search.

Additionally, the program should adhere to:

* Object-oriented design patterns.
* Test-driven development (TDD).
  * This includes unit and integration tests.
* When completing a module, submit a merge request from the branch that you are
  working on into master for review. Once the merge request is accepted, tag
  the commit with the name of the module and data structure (e.g.
  `1 - linked list`, `2 - stack`, etc...) and submit a pull request into the
  master branch.

The only requirements for each module are those enumerated above.

##### Module One

Complete the previous requirements by implementing a circularly linked list.

##### Module Two

Complete the previous requirements by implementing a doubly (and circularly)
linked list.

This should change the fields of the `card_node` structure and the `deck`
structure.

##### Module Three

Complete the previous requirements by implementing a stack.

This should change the fields of the `card_node` structure and `deck`
structure, and the behavior of the `draw` functionality so that the top card
from the deck is drawn, rather than the bottom.

##### Module Four

Complete the previous requirements by implementing a hash table. Rather than
truly hashing the new node, "bucket" them according to the first letter of the
title in the flashcard.

This should change the fields of the `card_node` structure and the `deck`
structure, and you should notice an increase in the efficiency of the `search`
functionality over the performance from modules one, two, and three.

The behavior of `draw` can be decided by the programmer as to whether it draws
from the top or the bottom of the deck.

##### Module Five

Complete the previous requirements by implementing a binary search tree.

This should change the fields of `card_node` structure and the `deck`
structure, and you should notice an increase in the efficiency of the `search`
functionality over the performance from modules one, two, and three.

The behavior of `draw` can be decided by the programmer as to whether it draws
from the top or the bottom of the deck.
